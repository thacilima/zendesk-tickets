//
//  URLSessionMock.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 17/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import Foundation

class URLSessionMock {
    
    class URLSessionDataTaskMock: URLSessionDataTask  {
        var completionHandlerData: (data: Data?, urlResponse: URLResponse?, error: Error?)
        
        typealias TaskCompletionHandler = (Data?, URLResponse?, Error?) -> Void
        var completionHandler: TaskCompletionHandler?
        
        init(data: Data?, response: URLResponse?, error: Error?) {
            completionHandlerData = (data, response, error)
        }
        
        override func resume() {
            DispatchQueue.main.async {
                self.completionHandler?(self.completionHandlerData.data, self.completionHandlerData.urlResponse, self.completionHandlerData.error)
            }
        }
    }
    
    private let dataTaskMock: URLSessionDataTaskMock
    
    init(data: Data?, response: URLResponse?, error: Error?) {
        dataTaskMock = URLSessionDataTaskMock(data: data, response: response, error: error)
    }
}

extension URLSessionMock: CustomURLSession {
    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        dataTaskMock.completionHandler = completionHandler
        return dataTaskMock
    }
}
