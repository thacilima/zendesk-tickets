//
//  Constant.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 13/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import UIKit

struct Constant {
    struct CellReuseIdentifier {
        static let ticketCell = "TicketTableViewCellReuseIdentifier"
        static let alertCell = "AlertMessageCellReuseIdentifier"
    }
    
    struct Color {
        static let primary = UIColor.init(named: "primary")!
        static let secondary = UIColor.init(named: "secondary")!
        static let auxiliar2 = UIColor.init(named: "auxiliar-2")!
    }
}
