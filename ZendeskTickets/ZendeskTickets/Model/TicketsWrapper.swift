//
//  TicketsWrapper.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 10/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import Foundation

struct TicketsWrapper: Codable {
    let tickets: [Ticket]
}
