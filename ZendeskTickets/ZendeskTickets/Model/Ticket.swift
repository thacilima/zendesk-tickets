//
//  Ticket.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 10/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import Foundation

struct Ticket: Codable {
    
    enum CodingKeys: String, CodingKey {
        case ticketId = "id"
        case subject
        case description
        case status
    }
    
    let ticketId: Int
    let subject: String
    let description: String
    let status: String
}
