//
//  CustomURLSession.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 17/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import Foundation

protocol CustomURLSession {
    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
}

extension URLSession: CustomURLSession { }
