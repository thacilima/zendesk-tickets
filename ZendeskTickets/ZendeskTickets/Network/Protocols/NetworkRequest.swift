//
//  NetworkRequest.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 14/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import Foundation

enum NetworkRequestError: Error {
    case nilDataFromResponse
}

protocol NetworkRequest: class {
    associatedtype Model
    
    var session: CustomURLSession { get set }
    
    func load(completion: @escaping (Model) -> Void, errorCompletion: @escaping (Error) -> Void)
    func decode(_ data: Data) throws -> Model
}

extension NetworkRequest {
    
    func load(urlRequest: URLRequest, completion: @escaping (Model) -> Void, errorCompletion: @escaping (Error) -> Void) {
        
        let task = session.dataTask(with: urlRequest, completionHandler: { [weak self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            guard error == nil else {
                errorCompletion(error!)
                return
            }
            
            guard let data = data else {
                errorCompletion(NetworkRequestError.nilDataFromResponse)
                return
            }
            
            do {
                if let this = self {
                    let decodedData = try this.decode(data)
                    completion(decodedData)
                }
                
            } catch {
                errorCompletion(error)
            }
        })
        task.resume()
    }
}
