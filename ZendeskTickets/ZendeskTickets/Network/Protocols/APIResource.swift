//
//  APIResource.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 14/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import Foundation

protocol APIResource {
    associatedtype Model: Codable
    var methodPath: String { get }
}

extension APIResource {
    var urlRequest: URLRequest {
        let username = "acooke+techtest@zendesk.com"
        let password = "mobile"
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        
        let loginString = "\(username):\(password)"
        let loginData = loginString.data(using: .utf8)!
        let loginBase64 = loginData.base64EncodedString()
        urlRequest.addValue("Basic \(loginBase64)", forHTTPHeaderField: "Authorization")
        
        return urlRequest
    }
    
    var url: URL {
        let host = "https://mxtechtest.zendesk.com"
        let urlString = "\(host)\(methodPath)"
        return URL(string: urlString)!
    }
}
