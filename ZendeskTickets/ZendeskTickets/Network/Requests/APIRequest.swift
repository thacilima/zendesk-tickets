//
//  APIRequest.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 15/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import Foundation

class APIRequest<Resource: APIResource> : NetworkRequest {
    let resource: Resource
    var session: CustomURLSession = URLSession.shared
    
    init(resource: Resource) {
        self.resource = resource
    }
    
    func load(completion: @escaping (Resource.Model) -> Void, errorCompletion: @escaping (Error) -> Void) {
        load(urlRequest: resource.urlRequest, completion: { resourceModel in
            DispatchQueue.main.async {
                completion(resourceModel)
            }
        }, errorCompletion: { error in
            DispatchQueue.main.async {
                errorCompletion(error)
            }
        })
    }
    
    func decode(_ data: Data) throws -> Resource.Model {
        do {
            let decodedData = try JSONDecoder().decode(Resource.Model.self, from: data)
            return decodedData
        } catch {
            throw error
        }
    }
}
