//
//  TicketsResource.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 14/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import Foundation

struct TicketsResource: APIResource {
    typealias Model = TicketsWrapper
    let methodPath = "/api/v2/views/{id}/tickets.json".replacingOccurrences(of: "{id}", with: "39551161")
}
