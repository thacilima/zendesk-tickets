//
//  AlertMessageTableViewCell.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 17/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import UIKit

class AlertMessageTableViewCell: UITableViewCell {
    @IBOutlet var alertIndicatorLabel: UILabel!
    @IBOutlet var alertMessageLabel: UILabel!
}
