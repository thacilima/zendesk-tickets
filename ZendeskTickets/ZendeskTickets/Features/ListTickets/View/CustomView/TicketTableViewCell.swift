//
//  TicketTableViewCell.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 13/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import UIKit

class TicketTableViewCell: UITableViewCell {

    @IBOutlet var cardView: UIView!
    @IBOutlet var leftLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet var rightLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lightShadow(view: cardView)
    }
    
    private func lightShadow(view: UIView) {
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2
    }
}
