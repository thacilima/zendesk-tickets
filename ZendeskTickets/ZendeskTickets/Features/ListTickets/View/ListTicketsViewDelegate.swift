//
//  ListTicketsViewDelegate.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 12/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import UIKit

protocol ListTicketsViewDelegate: class {
    func startLoading()
    func stopLoading()
    func show(tickets: [Ticket])
    func showError()
}
