//
//  ViewController.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 10/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import UIKit

class ListTicketsViewController: UIViewController {

    private var presenter: ListTicketsPresenter!
    
    @IBOutlet private var tableView: UITableView!
    private weak var refreshControl: UIRefreshControl!
    
    private var ticketTableViewCellDataList: [Ticket.TicketTableViewCellData]? = nil
    
    // MARK: - Inits
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        presenter = ListTicketsPresenter(viewDelegate: self)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        presenter = ListTicketsPresenter(viewDelegate: self)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRefreshControl()
        presenter.loadTickets()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Setups
    
    func setupRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefreshData(_:)), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        self.refreshControl = refreshControl
    }
    
    // MARK: - Listeners
    
    @objc func handleRefreshData(_ refreshControl: UIRefreshControl) {
        presenter.loadTickets()
    }
    
}

// MARK: - ListTicketsViewDelegate

extension ListTicketsViewController: ListTicketsViewDelegate {
    func startLoading() {
        refreshControl.beginRefreshing()
    }
    
    func stopLoading() {
        refreshControl.endRefreshing()
    }
    
    func show(tickets: [Ticket]) {
        ticketTableViewCellDataList = tickets.map { $0.ticketTableViewCellData }
        tableView.reloadData()
    }
    
    func showError() {
        ticketTableViewCellDataList = []
        tableView.reloadData()
    }
}

// MARK: - UITableViewDataSource

extension ListTicketsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let ticketTableViewCellDataList = ticketTableViewCellDataList else {
            return 0
        }
        
        if ticketTableViewCellDataList.count == 0 {
            return 1
        }
        return ticketTableViewCellDataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let ticketTableViewCellDataList = ticketTableViewCellDataList else {
            return UITableViewCell()
        }
        
        if ticketTableViewCellDataList.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier:Constant.CellReuseIdentifier.alertCell) as! AlertMessageTableViewCell
            
            cell.alertMessageLabel.text = "No tickets available or an error occurred."
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier:Constant.CellReuseIdentifier.ticketCell) as! TicketTableViewCell
            
            let ticketTableViewCellData = ticketTableViewCellDataList[indexPath.row]
            cell.leftLabel.text = ticketTableViewCellData.leftDetail
            cell.titleLabel.text = ticketTableViewCellData.title
            cell.subtitleLabel.text = ticketTableViewCellData.detail
            cell.rightLabel.text = ticketTableViewCellData.rightDetail
            cell.rightLabel.textColor = ticketTableViewCellData.rightIndicatorColor
            
            return cell
        }
    }
}

