//
//  ListTicketsPresenter.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 12/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import Foundation

class ListTicketsPresenter {
    private weak var viewDelegate: ListTicketsViewDelegate?
    private var ticketsAPIRequest = APIRequest(resource: TicketsResource())
    
    init(viewDelegate: ListTicketsViewDelegate) {
        self.viewDelegate = viewDelegate
    }
    
    // MARK: - Public
    
    func loadTickets() {
        viewDelegate?.startLoading()
        
        ticketsAPIRequest.load(completion: { [weak self] ticketWrapper in
            self?.viewDelegate?.stopLoading()
            self?.viewDelegate?.show(tickets: ticketWrapper.tickets)
        }, errorCompletion: { [weak self] error in
             self?.viewDelegate?.stopLoading()
             self?.viewDelegate?.showError()
        })
    }
}
