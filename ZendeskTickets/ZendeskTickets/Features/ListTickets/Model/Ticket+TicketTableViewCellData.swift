//
//  Ticket+TicketTableViewCellData.swift
//  ZendeskTickets
//
//  Created by Thaciana Lima on 13/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import UIKit

extension Ticket {
    typealias TicketTableViewCellData = (leftDetail: String, title: String, detail: String, rightIndicatorColor: UIColor, rightDetail: String)
    
    var ticketTableViewCellData: TicketTableViewCellData {
        return ("\(ticketId)", subject, description, indicatorColor(status: status), status)
    }
    
    func indicatorColor(status: String) -> UIColor {
        var color: UIColor!
        
        switch status {
        case "open":
            color = Constant.Color.secondary
            break
        case "pending":
            color = Constant.Color.auxiliar2
            break
        default:
            color = Constant.Color.primary
        }
        
        return color
    }
}
