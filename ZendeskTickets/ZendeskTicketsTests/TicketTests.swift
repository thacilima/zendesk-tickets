//
//  TicketTests.swift
//  ZendeskTicketsTests
//
//  Created by Thaciana Lima on 17/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import XCTest
@testable import ZendeskTickets

class TicketTests: XCTestCase {
    
    var ticketUnderTest: Ticket! = Ticket(ticketId: 1, subject: "Subject", description: "Description", status: "open")
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        ticketUnderTest = nil
        super.tearDown()
    }
    
    func test_TicketTableViewCellData() {
        //given

        //when
        let ticketTableViewCellData = ticketUnderTest.ticketTableViewCellData
        
        //then
        XCTAssertEqual(ticketUnderTest.subject, ticketTableViewCellData.title)
        XCTAssertEqual(ticketUnderTest.description, ticketTableViewCellData.detail)
        XCTAssertEqual("\(ticketUnderTest.ticketId)", ticketTableViewCellData.leftDetail)
        XCTAssertEqual(ticketUnderTest.status, ticketTableViewCellData.rightDetail)
        XCTAssertEqual(ticketUnderTest.indicatorColor(status: ticketUnderTest.status), ticketTableViewCellData.rightIndicatorColor)
    }
    
    func test_DiffenrentIndicatorColorForDifferentStatus() {
        //given
        let status1 = "open"
        let status2 = "pending"
        let status3 = "new"
        
        //when
        let colorStatus1 = ticketUnderTest.indicatorColor(status: status1)
        let colorStatus2 = ticketUnderTest.indicatorColor(status: status2)
        let colorStatus3 = ticketUnderTest.indicatorColor(status: status3)
        
        
        //then
        XCTAssertNotEqual(colorStatus1, colorStatus2)
        XCTAssertNotEqual(colorStatus1, colorStatus3)
        XCTAssertNotEqual(colorStatus2, colorStatus3)
    }
}
