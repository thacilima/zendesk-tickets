//
//  NetworkRequestTests.swift
//  ZendeskTicketsTests
//
//  Created by Thaciana Lima on 17/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import XCTest
@testable import ZendeskTickets

class NetworkRequestTests: XCTestCase {
    
    struct SUTModel { }
    
    class SUT: NetworkRequest {
        var session: CustomURLSession
        var didCallDecode = false
        
        init(session: CustomURLSession) {
            self.session = session
        }
        
        func decode(_ data: Data) throws -> SUTModel {
            didCallDecode = true
            return SUTModel()
        }
        
        func load(completion: @escaping (NetworkRequestTests.SUTModel) -> Void, errorCompletion: @escaping (Error) -> Void) {
            let url = URL(string: "https://www.google.com")!
            let urlRequest = URLRequest(url: url)
            load(urlRequest: urlRequest, completion: completion, errorCompletion: errorCompletion)
        }
    }
    
    class DecodeErrorSUT: SUT {
        override func decode(_ data: Data) throws -> SUTModel {
            didCallDecode = true
            throw NSError()
        }
    }
 
    var sut: SUT!
    var url: URL!
    var urlResponseSuccess: URLResponse!
    var urlResponseError: URLResponse!
    
    override func setUp() {
        super.setUp()
        url = URL(string: "https://www.google.com")!
        urlResponseSuccess = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
        urlResponseError = HTTPURLResponse(url: url, statusCode: 502, httpVersion: nil, headerFields: nil)
    }
    
    override func tearDown() {
        sut = nil
        url = nil
        urlResponseSuccess = nil
        urlResponseError = nil
        super.tearDown()
    }
    
    func test_Load_Success() {
        //given
        sut = SUT(session: URLSessionMock(data: Data(), response: urlResponseSuccess, error: nil))
        let promise = expectation(description: "Completion or error called")
        
        //when
        sut.load(completion: { (model) in
            promise.fulfill()
        }) { (error) in
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        //then
        XCTAssertTrue(sut.didCallDecode)
    }
    
    func test_Load_Error() {
        //given
        sut = SUT(session: URLSessionMock(data: nil, response: urlResponseError, error: NSError()))
        var didCallErrorCallback = false
        let promise = expectation(description: "Completion or error called")
        
        //when
        sut.load(completion: { (model) in
            promise.fulfill()
        }) { (error) in
            didCallErrorCallback = true
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        //then
        XCTAssertTrue(didCallErrorCallback)
    }
    
    func test_Load_NilDataError() {
        //given
        sut = SUT(session: URLSessionMock(data: nil, response: urlResponseSuccess, error: nil))
        var didCallErrorCallback = false
        var returnedError: Error? = nil
        let promise = expectation(description: "Completion or error called")
        
        //when
        sut.load(completion: { (model) in
            promise.fulfill()
        }) { (error) in
            didCallErrorCallback = true
            returnedError = error
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        //then
        XCTAssertTrue(didCallErrorCallback)
        let networkRequestError = returnedError as? NetworkRequestError
        XCTAssertNotNil(networkRequestError)
        XCTAssertEqual(networkRequestError! , NetworkRequestError.nilDataFromResponse)
        
    }
    
    func test_Load_DecodeError() {
        //given
        sut = DecodeErrorSUT(session: URLSessionMock(data: Data(), response: urlResponseSuccess, error: nil))
        var didCallErrorCallback = false
        let promise = expectation(description: "Completion or error called")
        
        //when
        sut.load(completion: { (model) in
            promise.fulfill()
        }) { (error) in
            didCallErrorCallback = true
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        //then
        XCTAssertTrue(didCallErrorCallback)
        XCTAssertTrue(sut.didCallDecode)
    }
}
