//
//  APIRequest+TicketsResourceTests.swift
//  ZendeskTicketsTests
//
//  Created by Thaciana Lima on 17/06/18.
//  Copyright © 2018 TL. All rights reserved.
//

import XCTest
@testable import ZendeskTickets

class APIRequest_TicketsResourceTests: XCTestCase {
    
    var apiRequestUnderTest: APIRequest<TicketsResource>!
    let ticketsResource = TicketsResource()
    
    override func setUp() {
        super.setUp()
        apiRequestUnderTest = APIRequest<TicketsResource>(resource: ticketsResource)
    }
    
    override func tearDown() {
        apiRequestUnderTest = nil
        super.tearDown()
    }
    
    func test_Decode_Success() {
        //given
        let testBundle = Bundle(for: type(of: self))
        let path = testBundle.path(forResource: "TicketsResourceResponse", ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
        
        //when
        XCTAssertNoThrow(try apiRequestUnderTest.decode(data))
        let ticketsWrapper = try! apiRequestUnderTest.decode(data)
        
        //then
        XCTAssertEqual(ticketsWrapper.tickets.count, 5)        
    }
    
    func test_Decode_Error() {
        //given
        let data = Data()
        
        //when, then
        XCTAssertThrowsError(try apiRequestUnderTest.decode(data))
    }
    
    func test_Load_Success() {
        //given
        let testBundle = Bundle(for: type(of: self))
        let path = testBundle.path(forResource: "TicketsResourceResponse", ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
        apiRequestUnderTest.session = URLSessionMock(data:  data, response: HTTPURLResponse(url: ticketsResource.url, statusCode: 200, httpVersion: nil, headerFields: nil), error: nil)
        let promise = expectation(description: "Completion or error called")
        var ticketsCount = 0
        
        //when
        apiRequestUnderTest.load(completion: { (ticketsWrapper) in
            ticketsCount = ticketsWrapper.tickets.count
            promise.fulfill()
        }) { (error) in
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        //then
        XCTAssertEqual(ticketsCount, 5)
    }
    
    func test_Load_Error() {
        //given
        apiRequestUnderTest.session = URLSessionMock(data:  Data(), response: HTTPURLResponse(url: ticketsResource.url, statusCode: 200, httpVersion: nil, headerFields: nil), error: nil)
        let promise = expectation(description: "Completion or error called")
        var didCallError = false
        
        //when
        apiRequestUnderTest.load(completion: { (ticketsWrapper) in
            promise.fulfill()
        }) { (error) in
            didCallError = true
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        //then
        XCTAssertTrue(didCallError)
    }
}
